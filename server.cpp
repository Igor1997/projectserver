#include "server.h"

Server::Server(QObject *parent) : QObject(parent) // конструктор
{
    server = new QTcpServer(this); // создали объект класса сервер

    connect(server, SIGNAL(newConnection()), this, SLOT(newConnection())); // что делать когда подключился новый клиент

    if(!server->listen(QHostAddress::Any, 9999))
    {
        qDebug() << "Server could not start";
    }
    else
    {
        qDebug() << "Server started!";
    }
}

equation Server::solveEquation(equation eq)
{
    float discriminant, realPart, imaginaryPart, x1, x2;
    struct equation temp;
    if (eq.a.toInt() == 0) {
        temp.a = "There is";
        temp.b = "no";
        temp.c = "roots";
        return temp;
    } else {
        discriminant = eq.b.toInt()*eq.b.toInt() - 4*eq.a.toInt()*eq.c.toInt();
        if (discriminant > 0) {
            x1 = (-eq.b.toInt() + sqrt(discriminant)) / (2*eq.a.toInt());
            x2 = (-eq.b.toInt() - sqrt(discriminant)) / (2*eq.a.toInt());
            temp.a = QString::number(x1);
            temp.b = QString::number(x2);
            temp.c = "real";
            return temp;
        } else if (discriminant == 0) {
            x1 = (-eq.b.toInt() + sqrt(discriminant)) / (2*eq.a.toInt());
            temp.a = temp.b = QString::number(x1);
            temp.c = "same";
            return temp;
        }else {
            realPart = (float) -eq.b.toInt()/(2*eq.a.toInt());
            imaginaryPart =sqrt(-discriminant)/(2*eq.a.toInt());
            temp.a = QString::number(realPart) + "+" + QString::number(imaginaryPart) + "i";
            temp.b = QString::number(realPart) + "-" + QString::number(imaginaryPart) + "i";
            temp.c = "complex";
            return temp;
        }
    }
}

    void Server::newConnection() // аналог как у клиента
    {
        socket = server->nextPendingConnection();

        socket->write("Hello, client");
        socket->flush();
        socket->waitForReadyRead();

        qDebug() << "Server reading greeting...";
        qDebug() << socket->readAll();

        connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));

    }

    void Server::readyRead()
    {
        qDebug() << "Server reading equation...";
        QDataStream in(socket->readAll());
        in >> equations;

        for (auto& eq : equations)
        {
            eq = solveEquation(eq);
        }

        qDebug() << "Server sending answer...";
        QByteArray bytesArray;
        QDataStream out(&bytesArray ,QIODevice::WriteOnly);
        out << equations;
        socket->write(bytesArray);
    }
