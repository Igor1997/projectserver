#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>
#include <QDebug>
#include <QDataStream>

struct equation
{
    QString a;
    QString b;
    QString c;
};

inline QDataStream &operator<<(QDataStream &out, const equation &e)
{
    out << e.a << e.b << e.c;
    return out;
}

inline QDataStream &operator>>(QDataStream &in, equation &e)
{
    in >> e.a >> e.b >> e.c;
    return in;
}

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = nullptr);

    equation solveEquation(equation eq); // метод который решает выражение! он получает коэффициенты квадратного уравнения

signals:

public slots:
    void newConnection(); // клиент подключился и мы готовы что-то прочитать
    void readyRead();

private:
    QTcpServer *server;
    QTcpSocket *socket;
    QVector<equation> equations;
};

#endif // SERVER_H
